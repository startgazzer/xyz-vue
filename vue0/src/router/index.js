import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Table from '@/components/Table'
import Buttons from '@/components/Buttons'
import List from '@/components/List'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/table',
      name: 'table',
      component: Table
    },
    {
      path: '/buttons',
      name: 'buttons',
      component: Buttons
    },
    {
      path: '/list',
      name: 'list',
      component: List
    },
    
  ]
})
